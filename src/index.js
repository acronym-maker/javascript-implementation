/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const AcronymMaker = require('./maker');

const {
    GreedyOrderedMatchingStrategy,
    RegexBasedOrderedMatchingStrategy,
    UnorderedMatchingStrategy
} = require('./matching');

const {
    selectAllLetters,
    selectFirstLetter
} = require('./selection');

/**
 * Associates the name of each selection strategy to the corresponding strategy.
 */
const SELECTION_STRATEGIES = {
    'all': selectAllLetters,
    'first': selectFirstLetter
};

/**
 * Associates the name of each matching strategy to the corresponding strategy.
 */
const MATCHING_STRATEGIES = {
    'ordered': RegexBasedOrderedMatchingStrategy,
    'ordered-greedy': GreedyOrderedMatchingStrategy,
    'unordered': UnorderedMatchingStrategy
};

/**
 * Creates an AcronymMaker instance that is properly set up w.r.t. the given
 * options.
 *
 * @param {object} options The options describing how to set up the maker.
 * @param {function(Acronym): void} callback The callback function to invoke
 *        each time an acronym is identified.
 * @param {function(any): string[]} [dictionaryToArray] The function transforming a
 *        dictionary into the expected format, i.e, an array of strings.
 *        By default, no transformation is performed.
 *
 * @returns {AcronymMaker} The AcronymMaker instance that has been set up.
 */
function createAcronymMaker(options, callback, dictionaryToArray = (d) => d) {
    // Getting the configuration for the maker.
    const selectionStrategy = SELECTION_STRATEGIES[options['select-letters']];
    const matchingStrategy = MATCHING_STRATEGIES[options['matching-strategy']];
    const maxConsecutiveUnused = options['max-consecutive-unused'];
    const maxTotalUnused = options['max-total-unused'];

    // Initializing the instance of AcronymMaker.
    const acronymMaker = new AcronymMaker(selectionStrategy,
        new matchingStrategy(maxConsecutiveUnused, maxTotalUnused), // eslint-disable-line new-cap
        callback);

    // Feeding the maker with the words from the specified dictionaries.
    for (const dictionary of options['dictionary']) {
        acronymMaker.addKnownWords(dictionaryToArray(dictionary));
    }

    // Returning the maker that has been set up.
    return acronymMaker;
}

// Making globally available the entry point of AcronymMaker.
// This is particularly useful for using the "browserified" version of this library.
global.createAcronymMaker = createAcronymMaker;
global.matchings = Object.keys(MATCHING_STRATEGIES);
global.selections = Object.keys(SELECTION_STRATEGIES);
