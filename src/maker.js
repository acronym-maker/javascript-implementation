/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const { TokenBuilder } = require('./token');

/**
 * AcronymMaker is responsible for dispatching the computation of acronyms to
 * the appropriate strategies.
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
module.exports = class AcronymMaker {

    /**
     * The letter selection strategy to use to select letters from the words
     * to create acronyms from.
     *
     * @type function(string): Set<string>
     */
    _selectionStrategy;

    /**
     * The matching strategy to use to check whether a word matches as an
     * acronym.
     *
     * @type MatchingStrategy
     */
    _matchingStrategy;

    /**
     * The callback function to invoke when an acronym is found.
     *
     * @type function(Acronym): void
     */
    _callback;

    /**
     * The dictionary of the words that are allowed to be used as acronyms.
     *
     * @type string[]
     */
    _knownWords;

    /**
     * Creates a new AcronymMaker.
     *
     * @param {function(string): Set<string>} selectionStrategy The letter
     *        selection strategy to use to select letters from the words
     *        to create acronyms from.
     * @param {MatchingStrategy} matchingStrategy The matching strategy to
     *        use to check whether a word matches as an acronym.
     * @param {function(Acronym): void} callback The callback function to
     *        invoke when an acronym is found.
     */
    constructor(selectionStrategy, matchingStrategy, callback) {
        this._selectionStrategy = selectionStrategy;
        this._matchingStrategy = matchingStrategy;
        this._callback = callback;
        this._knownWords = [];
    }

    /**
     * Adds the given word to the dictionary of words that are known by this
     * AcronymMaker.
     *
     * @param {string} word The word to add.
     */
    addKnownWord(word) {
        let normalizedWord = word.trim();
        normalizedWord = normalizedWord.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
        normalizedWord = normalizedWord.toLowerCase();
        this._knownWords.push(normalizedWord);
    }

    /**
     * Adds the given words to the dictionary of words that are known by this
     * AcronymMaker.
     *
     * @param {Iterable<string>} words The words to add.
     */
    addKnownWords(words) {
        for (const word of words) {
            this.addKnownWord(word);
        }
    }

    /**
     * Searches the known words to find acronyms for the given tokens.
     *
     * Tokens must be represented with a string having the following
     * format (question marks are used to mark optional tokens):
     *
     *     'word_1_1/.../word_1_n1[?]  ...  word_m_1/.../word_m_nm[?]'
     *
     * @param {string} tokens The tokens to find acronyms for, as a string in
     *        the format described above.
     */
    findAcronymsForString(tokens) {
        const extractedTokens = [];
        for (const currentToken of tokens.split(/\s+/)) {
            const tokenObject = { words: [], optional: false };
            let actualToken = currentToken;
            if (actualToken.endsWith('?')) {
                actualToken = actualToken.substring(0, actualToken.length - 1);
                tokenObject.optional = true;
            }
            for (const word of actualToken.split('/')) {
                tokenObject.words.push(word);
            }
            extractedTokens.push(tokenObject);
        }
        this.findAcronymsForObjects(extractedTokens);
    }

    /**
     * Searches the known words to find acronyms for the given tokens.
     *
     * Tokens must be represented with an array of objects.
     * Each of the objects must have a field "words" containing the words
     * of the corresponding token, and a field "optional" telling whether the
     * token is optional, as a Boolean value.
     *
     * @param {{optional: boolean, words: string}[]} tokens The tokens to find
     *        acronyms for, as an array in the format described above.
     */
    findAcronymsForObjects(tokens) {
        const extractedTokens = [];
        for (const currentToken of tokens) {
            const builder = new TokenBuilder(this._selectionStrategy);
            for (const word of currentToken.words) {
                builder.addWord(word);
            }
            builder.setOptional(currentToken.optional);
            extractedTokens.push(builder.build());
        }
        this.findAcronyms(extractedTokens);
    }

    /**
     * Searches the known words to find acronyms for the given Tokens.
     *
     * @param {Token[]} tokens The Tokens to find acronyms for.
     */
    findAcronyms(tokens) {
        this._matchingStrategy.setTokens(tokens);
        for (const word of this._knownWords) {
            const acronym = this._matchingStrategy.asAcronym(word);
            if (acronym !== null) {
                this._callback(acronym);
            }
        }
    }

};
