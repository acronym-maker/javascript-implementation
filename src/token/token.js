/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

/**
 * A Token represents a disjunction of words for which a letter has to appear
 * in the acronyms being created.
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
module.exports = class Token {

    /**
     * The words of the disjunction represented by this Token.
     *
     * @type string[]
     */
    _words;

    /**
     * The map associating each letter appearing in this Token to the word(s)
     * in which it appears.
     *
     * @type Map<string, Set<string>>
     */
    _matches;

    /**
     * Whether this Token is optional.
     *
     * @type boolean
     */
    _optional;

    /**
     * Creates a new Token.
     *
     * @param {string[]} words The words of the disjunction represented by
     *        the Token.
     * @param {Map<string, Set<string>>} matches The map associating each
     *        letter appearing in the Token to the word(s) in which it appears.
     * @param {boolean} optional Whether the Token is optional.
     */
    constructor(words, matches, optional) {
        this._words = words;
        this._matches = matches;
        this._optional = optional;
    }

    /**
     * Checks whether a letter can be used to represent this Token.
     *
     * @param {string} letter The letter to check.
     *
     * @returns Whether the letter appears in this Token.
     */
    contains(letter) {
        return this._matches.has(letter);
    }

    /**
     * Gives an iterable view of the letters appearing in this Token.
     *
     * @returns {IterableIterator<string>} The letters of this Token.
     */
    letters() {
        return this._matches.keys();
    }

    /**
     * Gives the set of words from this Token that contain the given letter.
     *
     * @param {string} letter The letter to find words for.
     *
     * @returns {Set<string>} The words in which the given letter appears.
     *
     * @throws {Error} If the letter does not appear in this Token.
     */
    wordsWith(letter) {
        if (this.contains(letter)) {
            return this._matches.get(letter);
        }
        throw new Error(`"${letter}" does not appear in ${this}`);
    }

    /**
     * Checks whether this Token is optional.
     *
     * @returns {boolean} If this Token is optional.
     */
    isOptional() {
        return this._optional;
    }

    /**
     * Gives a string representing the disjunction of the words contained in
     * this Token.
     *
     * @returns {string} A string representing this Token.
     */
    toString() {
        const string = this._words.join(' or ');
        if (this.isOptional()) {
            return `(${string})?`;
        }
        return `(${string})`;
    }

};
