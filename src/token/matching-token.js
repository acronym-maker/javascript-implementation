/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

/**
 * A MatchingToken represents a Token for which a matching letter exists in an
 * acronym.
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
module.exports = class MatchingToken {

    /**
     * The Token for which a matching letter exists.
     *
     * @type Token
     */
    _token;

    /**
     * The matching letter, which necessarily appears in the Token.
     *
     * @type string
     */
    _letter;

    /**
     * Creates a new MatchingToken.
     *
     * @param {Token} token The Token for which a matching letter exists.
     * @param {string} letter The matching letter, which has to appear in the Token.
     *
     * @throws {Error} If the letter does not appear in the Token.
     */
    constructor(token, letter) {
        if (!token.contains(letter)) {
            throw new Error(`"${letter}" does not appear in ${token}`);
        }

        this._token = token;
        this._letter = letter;
    }

    /**
     * Gives the explanations of the matching words appearing in the Token.
     * An explanation for a word is the string representation of this word,
     * in which all letters are lower-case characters, except the matching
     * letter which is upper-case.
     *
     * @returns {string[]} The array of explanations.
     */
    getExplanations() {
        // Looking for the matching words, which contain the letter.
        const matchingWords = this._token.wordsWith(this._letter);

        // Building the explanations.
        const explanations = [];
        for (const word of matchingWords) {
            explanations.push(this.getOneExplanation(word));
        }
        return explanations;
    }

    /**
     * Gives the explanation of the given word.
     *
     * @param {string} word The word to get the explanation of.
     *
     * @returns {string} The explanation of the word.
     */
    getOneExplanation(word) {
        const index = word.indexOf(this._letter);
        const { length } = this._letter;
        return word.substring(0, index) + this._letter.toUpperCase() + word.substring(index + length);
    }

};
