/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const Token = require('./token');

/**
 * A TokenBuilder makes easier the creation of a new instance of Token.
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
module.exports = class TokenBuilder {

    /**
     * The letter selection strategy to use to select letters from the
     * words of the built Token.
     *
     * @type function(string): Set<string>
     */
    _selectionStrategy;

    /**
     * The words of the disjunction represented by the built Token.
     *
     * @type string[]
     */
    _words;

    /**
     * The map associating each letter appearing in the built Token to the
     * word(s) in which it appears.
     *
     * @type Map<string, Set<string>>
     */
    _matches;

    /**
     * Whether the built Token is optional.
     *
     * @type boolean
     */
    _optional;

    /**
     * Creates a new TokenBuilder.
     *
     * @param {function(string): Set<string>} selectionStrategy The letter
     *        selection strategy to use to select letters from the words of
     *        the built Token.
     */
    constructor(selectionStrategy) {
        this._selectionStrategy = selectionStrategy;
        this._words = [];
        this._matches = new Map();
        this._optional = false;
    }

    /**
     * Adds a word to the built Token.
     *
     * @param {string} word The word to add.
     */
    addWord(word) {
        // Adding the word itself.
        let normalizedWord = word.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
        normalizedWord = normalizedWord.toLowerCase();
        this._words.push(normalizedWord);

        // Adding the matches between the word and its letters.
        for (const letter of this._selectionStrategy(normalizedWord)) {
            let matches = this._matches.get(letter);
            if (matches === undefined) {
                matches = new Set();
                this._matches.set(letter, matches);
            }
            matches.add(normalizedWord);
        }
    }

    /**
     * Sets the optionality of the built Token.
     *
     * @param {boolean} [optional] Whether the built Token is optional.
     *        If omitted, the Token is assumed to be optional.
     */
    setOptional(optional = true) {
        this._optional = optional;
    }

    /**
     * Creates the built Token.
     *
     * @returns {Token} The created Token.
     */
    build() {
        return new Token(this._words, this._matches, this._optional);
    }

};
