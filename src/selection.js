/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

/**
 * Checks whether a word is not empty, and throws an Error when this is not
 * the case.
 *
 * @param {string} word The word to check.
 *
 * @return {string} The given word, if and only if it is not empty.
 *
 * @throws {Error} If the given word is empty.
 */
function checkNotEmpty(word) {
    if (word !== '') {
        return word;
    }
    throw new Error('Cannot select letters from an empty word');
}

module.exports = {

    /**
     * Implements the letter selection strategy consisting in selecting only the
     * first letter of a word.
     *
     * @param {string} word The word to select letters from.
     *
     * @return {Set<string>} The set containing the first letter of the word.
     *
     * @throws {Error} If the given word is empty.
     */
    selectFirstLetter(word) {
        return new Set(checkNotEmpty(word)[0]);
    },

    /**
     * Implements the letter selection strategy consisting in selecting all the
     * letters of a word.
     *
     * @param {string} word The word to select letters from.
     *
     * @return {Set<string>} The set containing all the letters of the word.
     *
     * @throws {Error} If the given word is empty.
     */
    selectAllLetters(word) {
        return new Set(checkNotEmpty(word));
    }

};
