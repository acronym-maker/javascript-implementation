/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const MatchingStrategy = require('./matching-strategy');
const { Token } = require('../token');

/**
 * The RegexBasedOrderedMatchingStrategy checks whether a word can be
 * explained as an Acronym while preserving the order of the sequence
 * of Tokens, using regular expressions.
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
module.exports = class RegexBasedOrderedMatchingStrategy extends MatchingStrategy {

    /**
     * The regular expression defining the pattern of acronyms explained by
     * the associated sequence of Tokens.
     *
     * @type RegExp
     */
    _pattern;

    /**
     * Sets the Tokens for which this RegexBasedOrderedMatchingStrategy must
     * identify acronyms.
     *
     * @param {Token[]} tokens The Tokens to set.
     */
    setTokens(tokens) {
        super.setTokens(tokens);
        this._pattern = this.createPattern();
    }

    /**
     * Creates the pattern used to identify words matching as Acronyms.
     *
     * @returns {RegExp} The created pattern.
     */
    createPattern() {
        const unused = `(.{0,${this._maxConsecutiveUnused}}?)`;

        let regex = unused;
        for (const currentToken of this._tokens) {
            // Adding the set of the letters of the Token.
            regex += '(?:([';
            for (const letter of currentToken.letters()) {
                regex += letter;
            }
            regex += `])${unused})`;

            // Marking the set as optional if the Token is.
            if (currentToken.isOptional()) {
                regex += '?';
            }
        }

        return new RegExp(`^${regex}$`);
    }

    /**
     * Tries to fill in an explanation for the given word based on the
     * associated sequence of Tokens.
     *
     * @param {string} word The word to explain.
     * @param {(Token | null)[]} explanation The explanation to fill in.
     *        This array has the same length as word, and is initially
     *        filled with null.
     *        After invoking this method, the i-th element of this
     *        array will be a Token explaining the i-th letter of the
     *        word, or null if this letter is not explained.
     *
     * @returns {boolean} Whether an explanation has been found.
     *          If not, the content of explanation is undefined.
     */
    internalSearchExplanation(word, explanation) {
        // Checking whether the word matches as an acronym.
        const matches = word.match(this._pattern);
        if (!matches) {
            return false;
        }

        // Interpreting the groups of the regular expression as an explanation.
        this.explainAcronym(matches, explanation);
        return true;
    }

    /**
     * Explains a word that matches as an acronym based on the groups extracted
     * with the regular expression.
     *
     * @param {string[]} groups The groups identified by the regular expression.
     * @param {(Token | null)[]} explanation The explanation to fill in.
     *        This array has the same length as word, and is initially
     *        filled with null.
     *        After invoking this method, the i-th element of this
     *        array will be a Token explaining the i-th letter of the
     *        word, or null if this letter is not explained.
     */
    explainAcronym(groups, explanation) {
        let tokenIndex = 0;
        let charIndex = 0;

        for (let index = 1; index < groups.length; index += 1) {
            const group = groups[index];

            // Explaining a Token when the group corresponds to this Token.
            if (index % 2 === 0) {
                if (group) {
                    explanation[charIndex] = this._tokens[tokenIndex]; // eslint-disable-line no-param-reassign
                }
                tokenIndex += 1;
            }

            // Updating the current position in the word.
            if (group) {
                charIndex += group.length;
            }
        }
    }

};
