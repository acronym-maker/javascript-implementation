/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const MatchingStrategy = require('./matching-strategy');
const { Token } = require('../token');

/**
 * The GreedyOrderedMatchingStrategy checks whether a word can be explained
 * as an Acronym while preserving the order of the sequence of Tokens, using
 * a greedy algorithm.
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
module.exports = class GreedyOrderedMatchingStrategy extends MatchingStrategy {

    /**
     * Tries to fill in an explanation for the given word based on the
     * associated sequence of Tokens.
     *
     * @param {string} word The word to explain.
     * @param {(Token | null)[]} explanation The explanation to fill in.
     *        This array has the same length as word, and is initially
     *        filled with null.
     *        After invoking this method, the i-th element of this
     *        array will be a Token explaining the i-th letter of the
     *        word, or null if this letter is not explained.
     *
     * @returns {boolean} Whether an explanation has been found.
     *          If not, the content of explanation is undefined.
     */
    internalSearchExplanation(word, explanation) {
        // First, we try to use the mandatory Tokens.
        const tokenIndices = new Map();
        if (!this.explainMandatory(word, explanation, tokenIndices)) {
            return false;
        }

        // Then, we use the optional Tokens, when possible.
        this.explainOptional(word, explanation, tokenIndices);
        return true;
    }

    /**
     * Tries to find an explanation for the given word based on the
     * associated sequence of Tokens, considering only mandatory tokens.
     *
     * @param {string} word The word to explain.
     * @param {(Token | null)[]} explanation The explanation to fill in.
     *        This array has the same length as word, and is initially
     *        filled with null.
     *        After invoking this method, the i-th element of this
     *        array will be a Token explaining the i-th letter of the
     *        word, or null if this letter is not explained.
     * @param {Map<int, int>} tokenIndices The map storing, for each Token
     *        index, the index of the letter it explains in the word.
     *
     * @returns {boolean} Whether all mandatory tokens have been explained.
     *          If not, the content of explanation is undefined.
     */
    explainMandatory(word, explanation, tokenIndices) {
        // Looking for the first mandatory Token.
        let [tokenIndex, token] = this.nextMandatory(0);

        // Trying to explain each letter.
        for (let i = 0; (i < word.length) && (token !== null); i += 1) {
            // Trying to explain the current letter with the current Token.
            const letter = word[i];
            if (token.contains(letter)) {
                explanation[i] = token; // eslint-disable-line no-param-reassign
                tokenIndices.set(tokenIndex, i);
                [tokenIndex, token] = this.nextMandatory(tokenIndex + 1);
            }
        }

        // An explanation has been found if all mandatory Tokens have been used.
        return token === null;
    }

    /**
     * Looks for the next mandatory Token, starting from the given index.
     *
     * @param {int} index The index at which to start looking for.
     *
     * @returns {(int | Token | null)[]} An array containing the index of
     *          the next mandatory Token and this Token, or -1 and null if
     *          there is no more such Tokens.
     */
    nextMandatory(index) {
        // Looking for a mandatory Token.
        for (let i = index; i < this._tokens.length; i += 1) {
            const token = this._tokens[i];
            if (!token.isOptional()) {
                return [i, token];
            }
        }

        // No mandatory Token has been found.
        return [-1, null];
    }

    /**
     * Tries to find an explanation for the given word based on the
     * associated sequence of Tokens, considering only optional tokens.
     *
     * @param {string} word The word to explain.
     * @param {(Token | null)[]} explanation The explanation to fill in.
     *        This array has the same length as word, and is initially
     *        filled with null.
     *        After invoking this method, the i-th element of this
     *        array will be a Token explaining the i-th letter of the
     *        word, or null if this letter is not explained.
     * @param {Map<int, int>} tokenIndices The map storing, for each Token
     *        index, the index of the letter it explains in the word.
     */
    explainOptional(word, explanation, tokenIndices) {
        for (let tokenIndex = 0; tokenIndex < this._tokens.length; tokenIndex += 1) {
            // Only optional tokens are considered.
            const token = this._tokens[tokenIndex];
            if (!token.isOptional()) {
                continue;
            }

            // Looking for a letter to explain.
            const [lower, upper] = this.explainableLetters(word, tokenIndex, tokenIndices);
            for (let letterIndex = lower; letterIndex < upper; letterIndex += 1) {
                if (token.contains(word[letterIndex])) {
                    explanation[letterIndex] = token; // eslint-disable-line no-param-reassign
                    tokenIndices.set(tokenIndex, letterIndex);
                    break;
                }
            }
        }
    }

    /**
     * Computes the lower and upper bounds for the indices of the letters that
     * the (optional) Token at the given index can possibly explain.
     *
     * @param {string} word The word to explain.
     * @param {int} currentIndex The index of the current Token.
     * @param {Map<int, int>} tokenIndices The map storing, for each Token
     *        index, the index of the letter it explains in the word.
     *
     * @returns {int[]} The bounds for the possibly explainable letters.
     */
    explainableLetters(word, currentIndex, tokenIndices) {
        // The first letter that can be explained is that following the latest already explained.
        let lowerBound = 0;
        for (let index = currentIndex - 1; index >= 0; index -= 1) {
            const latestUsed = tokenIndices.get(index);
            if (latestUsed !== undefined) {
                lowerBound = latestUsed + 1;
                break;
            }
        }

        // The first non-explainable letter is that explained by the next mandatory Token.
        let upperBound = word.length;
        const nextMandatory = this.nextMandatory(currentIndex)[0];
        if (nextMandatory >= 0) {
            upperBound = tokenIndices.get(nextMandatory);
        }

        return [lowerBound, upperBound];
    }

};
