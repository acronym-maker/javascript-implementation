/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

/**
 * An Acronym represents a sequence of MatchingTokens combined so as to explain
 * a given word (the actual acronym).
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
module.exports = class Acronym {

    /**
     * The word that is explained, i.e., the actual acronym.
     *
     * @type string
     */
    _word;

    /**
     * The sequence of MatchingTokens explaining the word.
     *
     * @type MatchingToken[]
     */
    _matchingTokens;

    /**
     * Creates a new Acronym.
     *
     * @param {string} word The word that is explained, i.e., the actual acronym.
     * @param {MatchingToken[]} matchingTokens The sequence of MatchingTokens
     *        explaining the word.
     */
    constructor(word, matchingTokens) {
        this._word = word;
        this._matchingTokens = matchingTokens;
    }

    /**
     * Gives the word that this Acronym explains.
     * It is camel-cased so as to highlight the letters that are matched by
     * the associated MatchingTokens.
     *
     * @returns {string} The word that is explained.
     */
    getWord() {
        return this._word;
    }

    /**
     * Gives the explanations of this Acronym, i.e., the list of all possible
     * combinations of words that explain it.
     * The words in these explanations are camel-cased so as to highlight the
     * letters that appear in the explained word.
     *
     * @returns {string[]} The explanations of this Acronym.
     */
    getExplanations() {
        return this.combineTokens();
    }

    /**
     * Combines the associated MatchingTokens so as to create all the possible
     * explanations for this Acronym.
     *
     * @param {int} [index] The index at which to start exploring MatchingTokens.
     *         If omitted, the value 0 is assumed.
     *
     * @returns {string[]} The computed explanations.
     */
    combineTokens(index = 0) {
        if (index === (this._matchingTokens.length - 1)) {
            // Computing the explanations of the last Token.
            return this._matchingTokens[index].getExplanations();
        }

        // Computing the explanations of the rest of the tokens.
        const subExplanations = this.combineTokens(index + 1);

        // Combining them with the explanations of the current Token.
        const explanations = [];
        for (const explanation of this._matchingTokens[index].getExplanations()) {
            for (const subExplanation of subExplanations) {
                explanations.push(`${explanation} ${subExplanation}`);
            }
        }
        return explanations;
    }

    /**
     * Gives a string representation of this Acronym.
     * It is a multi-line string, in which each line is composed of the
     * actual acronym, followed by one possible explanation of this acronym.
     *
     * @returns {string} The string representation of this Acronym.
     */
    toString() {
        const explanations = [];
        for (const explanation of this.getExplanations()) {
            explanations.push(`${this._word}\t${explanation}`);
        }
        return explanations.join('\n');
    }

};
