/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const { hopcroftKarp } = require('hopcroft-karp');

const MatchingStrategy = require('./matching-strategy');
const { Token } = require('../token');

/**
 * The UnorderedMatchingStrategy checks whether a word can be explained as an
 * Acronym without considering the order of the sequence of Tokens, based on
 * the Hopcroft-Karp matching algorithm.
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
module.exports = class UnorderedMatchingStrategy extends MatchingStrategy {

    /**
     * The indices of the Tokens that are mandatory.
     *
     * @type int[]
     */
    _mandatoryTokens;

    /**
     * The indices of the Tokens that are optional.
     *
     * @type int[]
     */
    _optionalTokens;

    /**
     * Sets the Tokens for which this UnorderedMatchingStrategy must identify
     * acronyms.
     *
     * @param {Token[]} tokens The Tokens to set.
     */
    setTokens(tokens) {
        super.setTokens(tokens);
        this._mandatoryTokens = [];
        this._optionalTokens = [];

        for (let index = 0; index < tokens.length; index += 1) {
            const token = tokens[index];
            if (token.isOptional()) {
                this._optionalTokens.push(index);
            } else {
                this._mandatoryTokens.push(index);
            }
        }
    }

    /**
     * Tries to fill in an explanation for the given word based on the
     * associated sequence of Tokens.
     *
     * @param {string} word The word to explain.
     * @param {(Token | null)[]} explanation The explanation to fill in.
     *        This array has the same length as word, and is initially
     *        filled with null.
     *        After invoking this method, the i-th element of this
     *        array will be a Token explaining the i-th letter of the
     *        word, or null if this letter is not explained.
     *
     * @returns {boolean} Whether an explanation has been found.
     *          If not, the content of explanation is undefined.
     */
    internalSearchExplanation(word, explanation) {
        // First, looking for a matching of the mandatory tokens.
        const letters = new Set();
        for (let index = 0; index < word.length; index += 1) {
            letters.add(index);
        }
        const matching = this.findMaximumMatching(word, this._mandatoryTokens, letters);

        // If there is no complete matching, the word cannot be explained as an acronym.
        if (matching.size !== this._mandatoryTokens.length) {
            return false;
        }

        // Removing the letters that are explained by the mapping.
        for (const letterIndex of matching.keys()) {
            letters.delete(Number(letterIndex));
        }

        // Trying to use optional tokens to explain more letters.
        const matchingOptional = this.findMaximumMatching(word, this._optionalTokens, letters);
        for (const key of matchingOptional.keys()) {
            matching.set(key, matchingOptional.get(key));
        }

        // Interpreting the matching as an explanation.
        this.explainAcronym(word, matching, explanation);
        return true;
    }

    /**
     * Finds a maximum matching between a set of Tokens and a set of letters
     * from a word to explain.
     *
     * @param {string} word The word to explain.
     * @param {int[]} tokens The list of the indices of the Tokens to consider
     *        in the matching.
     * @param {Set<int>} letters The indices of the letters to consider in the
     *        matching.
     *
     * @returns Map<string, string> The maximum matching between the specified
     *          Tokens and the specified subset of the letters of the word.
     */
    findMaximumMatching(word, tokens, letters) {
        // Creating the bipartite graph to compute the maximum matching of.
        const graph = {};
        for (let index = 0; index < word.length; index += 1) {
            const letter = word[index];
            const strIndex = index.toString();
            if (letters.has(index)) {
                graph[strIndex] = this.getTokensWith(letter, tokens);
            }
        }

        // Actually computing the maximum matching.
        const matching = hopcroftKarp(graph);
        const matchingMap = new Map();
        for (const key of Object.keys(matching)) {
            if (matching[key] !== null) {
                matchingMap.set(key, matching[key]);
            }
        }
        return matchingMap;
    }

    /**
     * Searches for Tokens that can explain the given letter.
     *
     * @param {string} letter The letter to explain.
     * @param {int[]} tokens The list of the indices of the Tokens that may
     *        explain the letter.
     *
     * @returns {string[]} The set of the indices of the Tokens that explain
     *          the letter.
     */
    getTokensWith(letter, tokens) {
        const tokenIndices = [];
        for (const tokenIndex of tokens) {
            if (this._tokens[tokenIndex].contains(letter)) {
                tokenIndices.push(tokenIndex.toString());
            }
        }
        return tokenIndices;
    }

    /**
     * Explains a word that matches as an acronym based on the computed maximum
     * matching.
     *
     * @param {string} word The word to explain.
     * @param {Map<string, string>} matching The maximum matching that matches
     *        a Token to the index of the letter it explains in the word.
     * @param {(Token | null)[]} explanation The explanation to fill in.
     *        This array has the same length as word, and is initially
     *        filled with null.
     *        After invoking this method, the i-th element of this
     *        array will be a Token explaining the i-th letter of the
     *        word, or null if this letter is not explained.
     */
    explainAcronym(word, matching, explanation) {
        for (let index = 0; index < word.length; index += 1) {
            const tokenIndex = matching.get(index.toString());
            if (tokenIndex !== undefined) {
                explanation[index] = this._tokens[Number(tokenIndex)]; // eslint-disable-line no-param-reassign
            }
        }
    }

};
