/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const Acronym = require('./acronym');
const { MatchingToken, Token } = require('../token');

/**
 * The MatchingStrategy is the parent class of the strategies that check
 * whether a word can be explained as an Acronym by a sequence of Tokens.
 *
 * @author Romain Wallon
 *
 * @version 0.1.0
 */
module.exports = class MatchingStrategy {

    /**
     * Creates a new MatchingStrategy.
     *
     * @param {int} maxConsecutiveUnused The maximum number of consecutive
     *        unused letters in the acronyms (i.e., letters that do not match
     *        the Tokens).
     * @param {int} maxTotalUnused The maximum number of overall unused letters
     *        in the acronyms (i.e., letters that do not match the Tokens).
     */
    constructor(maxConsecutiveUnused, maxTotalUnused) {
        this._maxConsecutiveUnused = maxConsecutiveUnused;
        this._maxTotalUnused = maxTotalUnused;
        this._tokens = undefined;
    }

    /**
     * Sets the Tokens for which this MatchingStrategy must identify acronyms.
     *
     * @param {Token[]} tokens The Tokens to set.
     */
    setTokens(tokens) {
        this._tokens = tokens;
    }

    /**
     * Finds an Acronym that explains the given word based on the associated
     * sequence of Tokens.
     *
     * @param {string} word The word to explain.
     *
     * @returns {Acronym | null} The Acronym explaining the given word,
     *          or null if the word cannot be explained.
     *
     * @throws {Error} If the sequence of Tokens has not been set.
     */
    asAcronym(word) {
        if (this._tokens === undefined) {
            throw new Error('There are no tokens to find acronyms for');
        }

        const [acronym, matchingTokens] = this.searchExplanation(word);
        if (matchingTokens.length > 0) {
            return new Acronym(acronym, matchingTokens);
        }
        return null;
    }

    /**
     * Tries to find a combination of the associated Tokens explaining the
     * given word.
     *
     * @param {string} word The word to explain.
     *
     * @returns {(string | MatchingToken[])[]} An array containing the camel-cased
     *          word highlighting the matching letters, and the array of
     *          MatchingTokens explaining the word.
     *          If the latter array is empty, then the word cannot be explained
     *          with the associated Tokens.
     *          In this case, the content of the first string is undefined.
     */
    searchExplanation(word) {
        // Looking for a candidate explanation.
        const explanation = new Array(word.length).fill(null);
        if (!this.internalSearchExplanation(word, explanation)) {
            return [word, []];
        }

        // Building the Acronym, unless the explanation is invalidated.
        let acronym = '';
        const matchingTokens = [];
        let consecutiveUnused = 0;
        let totalUnused = 0;

        // Trying to explain the letters of the word.
        for (let index = 0; index < word.length; index += 1) {
            const letter = word[index];
            const currentToken = explanation[index];

            if (currentToken === null) {
                // The letter is not explained.
                acronym += letter;
                consecutiveUnused += 1;
                totalUnused += 1;

            } else {
                // The letter is explained by the current Token.
                acronym += letter.toUpperCase();
                consecutiveUnused = 0;
                matchingTokens.push(new MatchingToken(currentToken, letter));
            }

            if ((consecutiveUnused > this._maxConsecutiveUnused) || (totalUnused > this._maxTotalUnused)) {
                // Too many letters are not used: the explanation is invalidated.
                return [word, []];
            }
        }

        // The explanation is validated.
        return [acronym, matchingTokens];
    }

    /**
     * Tries to fill in an explanation for the given word based on the
     * associated sequence of Tokens.
     *
     * @param {string} word The word to explain.
     * @param {(Token | null)[]} explanation The explanation to fill in.
     *        This array has the same length as word, and is initially
     *        filled with null.
     *        After invoking this method, the i-th element of this
     *        array will be a Token explaining the i-th letter of the
     *        word, or null if this letter is not explained.
     *
     * @returns {boolean} Whether an explanation has been found.
     *          If not, the content of explanation is undefined.
     */
    internalSearchExplanation(word, explanation) {
        throw new TypeError('Method "internalSearchExplanation()" is abstract');
    }

};
