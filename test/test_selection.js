/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const assert = require('assert').strict;

const {
    selectAllLetters,
    selectFirstLetter
} = require('../src/selection');

/*
 * Test case for the letter selection strategy selecting only the first letter
 * from a given word.
 */
describe('First-letter selection strategy', () => {
    it('should throw an Error when given an empty word', () => {
        assert.throws(() => selectFirstLetter(''));
    });

    it('should select the expected letters from the word "alert"', () => {
        const letters = selectFirstLetter('alert');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('a'));
    });

    it('should select the expected letters from the word "bright"', () => {
        const letters = selectFirstLetter('bright');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('b'));
    });

    it('should select the expected letters from the word "frame"', () => {
        const letters = selectFirstLetter('frame');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('f'));
    });

    it('should select the expected letters from the word "horn"', () => {
        const letters = selectFirstLetter('horn');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('h'));
    });

    it('should select the expected letters from the word "hum"', () => {
        const letters = selectFirstLetter('hum');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('h'));
    });

    it('should select the expected letters from the word "mere"', () => {
        const letters = selectFirstLetter('mere');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('m'));
    });

    it('should select the expected letters from the word "skillful"', () => {
        const letters = selectFirstLetter('skillful');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('s'));
    });

    it('should select the expected letters from the word "spiffy"', () => {
        const letters = selectFirstLetter('spiffy');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('s'));
    });

    it('should select the expected letters from the word "wound"', () => {
        const letters = selectFirstLetter('wound');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('w'));
    });

    it('should select the expected letters from the word "yarn"', () => {
        const letters = selectFirstLetter('yarn');
        assert.equal(letters.size, 1);
        assert.ok(letters.has('y'));
    });
});

/*
 * Test case for the letter selection strategy selecting all the letters
 * from a given word.
 */
describe('All-letter selection strategy', () => {
    it('should throw an Error when given an empty word', () => {
        assert.throws(() => selectAllLetters(''));
    });

    it('should select the expected letters from the word "alert"', () => {
        const letters = selectAllLetters('alert');
        assert.equal(letters.size, 5);
        assert.ok(letters.has('a'));
        assert.ok(letters.has('l'));
        assert.ok(letters.has('e'));
        assert.ok(letters.has('r'));
        assert.ok(letters.has('t'));
    });

    it('should select the expected letters from the word "bright"', () => {
        const letters = selectAllLetters('bright');
        assert.equal(letters.size, 6);
        assert.ok(letters.has('b'));
        assert.ok(letters.has('r'));
        assert.ok(letters.has('i'));
        assert.ok(letters.has('g'));
        assert.ok(letters.has('h'));
        assert.ok(letters.has('t'));
    });

    it('should select the expected letters from the word "frame"', () => {
        const letters = selectAllLetters('frame');
        assert.equal(letters.size, 5);
        assert.ok(letters.has('f'));
        assert.ok(letters.has('r'));
        assert.ok(letters.has('a'));
        assert.ok(letters.has('m'));
        assert.ok(letters.has('e'));
    });

    it('should select the expected letters from the word "horn"', () => {
        const letters = selectAllLetters('horn');
        assert.equal(letters.size, 4);
        assert.ok(letters.has('h'));
        assert.ok(letters.has('o'));
        assert.ok(letters.has('r'));
        assert.ok(letters.has('n'));
    });

    it('should select the expected letters from the word "hum"', () => {
        const letters = selectAllLetters('hum');
        assert.equal(letters.size, 3);
        assert.ok(letters.has('h'));
        assert.ok(letters.has('u'));
        assert.ok(letters.has('m'));
    });

    it('should select the expected letters from the word "mere"', () => {
        const letters = selectAllLetters('mere');
        assert.equal(letters.size, 3);
        assert.ok(letters.has('m'));
        assert.ok(letters.has('e'));
        assert.ok(letters.has('r'));
    });

    it('should select the expected letters from the word "skillful"', () => {
        const letters = selectAllLetters('skillful');
        assert.equal(letters.size, 6);
        assert.ok(letters.has('s'));
        assert.ok(letters.has('k'));
        assert.ok(letters.has('i'));
        assert.ok(letters.has('l'));
        assert.ok(letters.has('f'));
        assert.ok(letters.has('u'));
    });

    it('should select the expected letters from the word "spiffy"', () => {
        const letters = selectAllLetters('spiffy');
        assert.equal(letters.size, 5);
        assert.ok(letters.has('s'));
        assert.ok(letters.has('p'));
        assert.ok(letters.has('i'));
        assert.ok(letters.has('f'));
        assert.ok(letters.has('y'));
    });

    it('should select the expected letters from the word "wound"', () => {
        const letters = selectAllLetters('wound');
        assert.equal(letters.size, 5);
        assert.ok(letters.has('w'));
        assert.ok(letters.has('o'));
        assert.ok(letters.has('u'));
        assert.ok(letters.has('n'));
        assert.ok(letters.has('d'));
    });

    it('should select the expected letters from the word "yarn"', () => {
        const letters = selectAllLetters('yarn');
        assert.equal(letters.size, 4);
        assert.ok(letters.has('y'));
        assert.ok(letters.has('a'));
        assert.ok(letters.has('r'));
        assert.ok(letters.has('n'));
    });
});
