/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const assert = require('assert').strict;
const itParam = require('mocha-param');

const {
    GreedyOrderedMatchingStrategy,
    RegexBasedOrderedMatchingStrategy,
    UnorderedMatchingStrategy
} = require('../src/matching');

const {
    selectAllLetters,
    selectFirstLetter
} = require('../src/selection');

const {
    TokenBuilder
} = require('../src/token');

/* ---------------- *
 * HELPER FUNCTIONS *
 * ---------------- */

/**
 * Creates a new Token.
 *
 * @param {function(string): Set<string>} selectionStrategy The letter selection
 *        strategy to use to build the Tokens.
 * @param {string[]} words The words of the Token to create.
 * @param {boolean} optional Whether the Token to create is optional.
 *
 * @return {Token} The created Token.
 */
function createToken(selectionStrategy, words, optional) {
    const builder = new TokenBuilder(selectionStrategy);
    for (const word of words) {
        builder.addWord(word);
    }
    builder.setOptional(optional);
    return builder.build();
}

/**
 * Creates the default Tokens, which are used in most test cases.
 *
 * @param {function(string): Set<string>} selectionStrategy The letter selection
 *        strategy to use to build the Tokens.
 *
 * @returns {Token[]} The array of the default Tokens.
 */
function createDefaultTokens(selectionStrategy) {
    const buildCreate = createToken(selectionStrategy, ['build', 'create'], false);
    const amazingAwesome = createToken(selectionStrategy, ['amazing', 'awesome'], false);
    const acronym = createToken(selectionStrategy, ['acronyms'], false);
    const optFor = createToken(selectionStrategy, ['for'], true);
    const optYour = createToken(selectionStrategy, ['your'], true);
    const projects = createToken(selectionStrategy, ['projects'], false);
    return [buildCreate, amazingAwesome, acronym, optFor, optYour, projects];
}

/**
 * Checks that an acronym is explained by the specified sequence of Tokens.
 *
 * @param {MatchingStrategy} matchingStrategy The matching strategy used to test whether the
 *        acronym is explained.
 * @param {Token[]} tokens The Tokens with which to perform the test.
 * @param {string} explainedAcronym The acronym that should be explained.
 * @param {string[]} explanations The explanations of the acronym.
 */
function assertMatches(matchingStrategy, tokens, explainedAcronym, explanations) {
    matchingStrategy.setTokens(tokens);
    const acronym = matchingStrategy.asAcronym(explainedAcronym.toLowerCase());
    assert.notEqual(acronym, null);

    assert.equal(acronym.getWord(), explainedAcronym);

    assert.equal(explanations.length, acronym.getExplanations().length);
    for (const explanation of explanations) {
        assert.ok(acronym.getExplanations().includes(explanation),
            `${explanation} is not in [${acronym.getExplanations()}]`);
    }
}

/**
 * Checks that the given word is not explained as an acronym by the specified sequence of Tokens.
 *
 * @param {MatchingStrategy} matchingStrategy The matching strategy used to test whether the word
 *        is explained.
 * @param {Token[]} tokens The Tokens with which to perform the test.
 * @param {string} word The word that should not be explained.
 */
function assertDoesNotMatch(matchingStrategy, tokens, word) {
    matchingStrategy.setTokens(tokens);
    const acronym = matchingStrategy.asAcronym(word.toLowerCase());
    assert.equal(acronym, null, `Word "${word}" is explained: ${acronym}`);
}

/* ---------- *
 * TEST CASES *
 * ---------- */

/*
 * Test case for checking matching strategies considering the order of the tokens combined
 * with the selection of the first letter of the words in the considered tokens.
 */
describe('An ordered-based matching strategy used with the first-letter selection strategy', () => {
    const tokens = createDefaultTokens(selectFirstLetter);
    const matchingStrategies = [GreedyOrderedMatchingStrategy, RegexBasedOrderedMatchingStrategy];

    itParam('should explain acronyms as expected', matchingStrategies, (strategy) => {
        const matchingStrategy = new strategy(10, 15); // eslint-disable-line new-cap
        assertMatches(matchingStrategy, tokens, 'CAnAPe',
            ['Create Awesome Acronyms Projects', 'Create Amazing Acronyms Projects']);
        assertMatches(matchingStrategy, tokens, 'CArAPace',
            ['Create Awesome Acronyms Projects', 'Create Amazing Acronyms Projects']);
        assertMatches(matchingStrategy, tokens, 'oCeAnogrAPhy',
            ['Create Awesome Acronyms Projects', 'Create Amazing Acronyms Projects']);
    });

    itParam('should not match when all mandatory tokens cannot be used', matchingStrategies, (strategy) => {
        const matchingStrategy = new strategy(5, 10); // eslint-disable-line new-cap
        assertDoesNotMatch(matchingStrategy, tokens, 'aaron');
        assertDoesNotMatch(matchingStrategy, tokens, 'army');
        assertDoesNotMatch(matchingStrategy, tokens, 'cataloguing');
        assertDoesNotMatch(matchingStrategy, tokens, 'cavity');
        assertDoesNotMatch(matchingStrategy, tokens, 'cold');
        assertDoesNotMatch(matchingStrategy, tokens, 'earn');
        assertDoesNotMatch(matchingStrategy, tokens, 'feature');
        assertDoesNotMatch(matchingStrategy, tokens, 'mail');
        assertDoesNotMatch(matchingStrategy, tokens, 'playback');
        assertDoesNotMatch(matchingStrategy, tokens, 'supersaturated');
        assertDoesNotMatch(matchingStrategy, tokens, 'telepathically');
        assertDoesNotMatch(matchingStrategy, tokens, 'unmusical');
        assertDoesNotMatch(matchingStrategy, tokens, 'welcome');
    });

    itParam('should not match when there are too many consecutive unexplained letters.', matchingStrategies, (strategy) => {
        const matchingStrategy = new strategy(1, 10); // eslint-disable-line new-cap
        assertDoesNotMatch(matchingStrategy, tokens, 'oceanography');
    });

    itParam('should not match when there are too many overall unexplained letters.', matchingStrategies, (strategy) => {
        const matchingStrategy = new strategy(3, 3); // eslint-disable-line new-cap
        assertDoesNotMatch(matchingStrategy, tokens, 'carapace');
    });
});

/*
 * Test case for checking the UnorderedMatchingStrategy combined with the selection of the first
 * letter of the words in the considered tokens.
 */
describe('The unordered-based matching strategy used with the first-letter selection strategy', () => {
    const tokens = createDefaultTokens(selectFirstLetter);

    it('should explain acronyms as expected', () => {
        const matchingStrategy = new UnorderedMatchingStrategy(5, 10);
        assertMatches(matchingStrategy, tokens, 'CAnAPe',
            ['Create Awesome Acronyms Projects', 'Create Amazing Acronyms Projects']);
        assertMatches(matchingStrategy, tokens, 'CArAPace',
            ['Create Awesome Acronyms Projects', 'Create Amazing Acronyms Projects']);
        assertMatches(matchingStrategy, tokens, 'oCeAnogrAPhY',
            ['Create Awesome Acronyms Projects Your', 'Create Amazing Acronyms Projects Your']);
        assertMatches(matchingStrategy, tokens, 'PlAYBAck',
            ['Projects Awesome Your Build Acronyms', 'Projects Amazing Your Build Acronyms']);
        assertMatches(matchingStrategy, tokens, 'telePAthiCAllY',
            ['Projects Awesome Create Acronyms Your', 'Projects Amazing Create Acronyms Your']);
    });

    it('should not match when all mandatory tokens cannot be used', () => {
        const matchingStrategy = new UnorderedMatchingStrategy(5, 10);
        assertDoesNotMatch(matchingStrategy, tokens, 'aaron');
        assertDoesNotMatch(matchingStrategy, tokens, 'army');
        assertDoesNotMatch(matchingStrategy, tokens, 'cataloguing');
        assertDoesNotMatch(matchingStrategy, tokens, 'cavity');
        assertDoesNotMatch(matchingStrategy, tokens, 'cold');
        assertDoesNotMatch(matchingStrategy, tokens, 'earn');
        assertDoesNotMatch(matchingStrategy, tokens, 'feature');
        assertDoesNotMatch(matchingStrategy, tokens, 'mail');
        assertDoesNotMatch(matchingStrategy, tokens, 'supersaturated');
        assertDoesNotMatch(matchingStrategy, tokens, 'unmusical');
        assertDoesNotMatch(matchingStrategy, tokens, 'welcome');
    });

    it('should not match when there are too many consecutive unexplained letters.', () => {
        const matchingStrategy = new UnorderedMatchingStrategy(1, 10);
        assertDoesNotMatch(matchingStrategy, tokens, 'oceanography');
        assertDoesNotMatch(matchingStrategy, tokens, 'supersaturated');
    });

    it('should not match when there are too many overall unexplained letters.', () => {
        const matchingStrategy = new UnorderedMatchingStrategy(3, 3);
        assertDoesNotMatch(matchingStrategy, tokens, 'carapace');
        assertDoesNotMatch(matchingStrategy, tokens, 'cataloguing');
        assertDoesNotMatch(matchingStrategy, tokens, 'supersaturated');
    });
});

/*
 * Test case for checking the GreedyOrderedMatchingStrategy combined with the selection of all the
 * letters of the words in the considered tokens.
 */
describe('The greedy ordered-based matching strategy used with the all-letters selection strategy', () => {
    const tokens = createDefaultTokens(selectAllLetters);

    it('should explain acronyms as expected', () => {
        const matchingStrategy = new GreedyOrderedMatchingStrategy(5, 10);
        assertMatches(matchingStrategy, tokens, 'CANaPe',
            ['Create Amazing acroNyms Projects', 'Create Awesome acroNyms Projects']);
        assertMatches(matchingStrategy, tokens, 'CARaPace',
            ['Create Amazing acRonyms Projects', 'Create Awesome acRonyms Projects']);
        assertMatches(matchingStrategy, tokens, 'fEAtuRE',
            ['crEate Amazing acRonyms projEcts', 'crEate Awesome acRonyms projEcts']);
        assertMatches(matchingStrategy, tokens, 'pLAYbaCk',
            ['buiLd Amazing acronYms projeCts', 'buiLd Awesome acronYms projeCts']);
        assertMatches(matchingStrategy, tokens, 'wElcOME',
            ['crEate awesOme acronyMs projEcts']);
        assertMatches(matchingStrategy, tokens, 'yEARbOoks',
            ['crEate Amazing acRonyms prOjects', 'crEate Awesome acRonyms prOjects']);
        assertMatches(matchingStrategy, tokens, 'zEAlOUSly',
            ['crEate Amazing acrOnyms yoUr projectS', 'crEate Awesome acrOnyms yoUr projectS']);
    });

    it('should not match when all mandatory tokens cannot be used', () => {
        const matchingStrategy = new GreedyOrderedMatchingStrategy(5, 10);
        assertDoesNotMatch(matchingStrategy, tokens, 'army');
        assertDoesNotMatch(matchingStrategy, tokens, 'cavity');
        assertDoesNotMatch(matchingStrategy, tokens, 'cold');
        assertDoesNotMatch(matchingStrategy, tokens, 'earn');
        assertDoesNotMatch(matchingStrategy, tokens, 'mail');
        assertDoesNotMatch(matchingStrategy, tokens, 'supersaturated');
    });

    it('should not match when there are too many consecutive unexplained letters.', () => {
        const matchingStrategy = new GreedyOrderedMatchingStrategy(1, 10);
        assertDoesNotMatch(matchingStrategy, tokens, 'carapace');
        assertDoesNotMatch(matchingStrategy, tokens, 'feature');
        assertDoesNotMatch(matchingStrategy, tokens, 'playback');
        assertDoesNotMatch(matchingStrategy, tokens, 'welcome');
        assertDoesNotMatch(matchingStrategy, tokens, 'yearbooks');
        assertDoesNotMatch(matchingStrategy, tokens, 'zealously');
    });

    it('should not match when there are too many overall unexplained letters.', () => {
        const matchingStrategy = new GreedyOrderedMatchingStrategy(3, 3);
        assertDoesNotMatch(matchingStrategy, tokens, 'carapace');
        assertDoesNotMatch(matchingStrategy, tokens, 'playback');
        assertDoesNotMatch(matchingStrategy, tokens, 'yearbooks');
        assertDoesNotMatch(matchingStrategy, tokens, 'zealously');
    });
});

/*
 * Test case for checking the RegexBasedOrderedMatchingStrategy combined with the selection of all
 * the letters of the words in the considered tokens.
 */
describe('The regex ordered-based matching strategy used with the all-letters selection strategy', () => {
    const tokens = createDefaultTokens(selectAllLetters);

    it('should explain acronyms as expected', () => {
        const matchingStrategy = new RegexBasedOrderedMatchingStrategy(5, 10);
        assertMatches(matchingStrategy, tokens, 'CANaPe',
            ['Create Amazing acroNyms Projects', 'Create Awesome acroNyms Projects']);
        assertMatches(matchingStrategy, tokens, 'CARaPace',
            ['Create Amazing acRonyms Projects', 'Create Awesome acRonyms Projects']);
        assertMatches(matchingStrategy, tokens, 'pLAYbaCk',
            ['buiLd Amazing acronYms projeCts', 'buiLd Awesome acronYms projeCts']);
        assertMatches(matchingStrategy, tokens, 'wElcOME',
            ['crEate awesOme acronyMs projEcts']);
        assertMatches(matchingStrategy, tokens, 'yEARbOOkS',
            ['crEate Amazing acRonyms fOr yOur projectS', 'crEate Awesome acRonyms fOr yOur projectS']);
        assertMatches(matchingStrategy, tokens, 'zEAlOUSly',
            ['crEate Amazing acrOnyms yoUr projectS', 'crEate Awesome acrOnyms yoUr projectS']);
    });

    it('should not match when all mandatory tokens cannot be used', () => {
        const matchingStrategy = new RegexBasedOrderedMatchingStrategy(5, 10);
        assertDoesNotMatch(matchingStrategy, tokens, 'army');
        assertDoesNotMatch(matchingStrategy, tokens, 'cavity');
        assertDoesNotMatch(matchingStrategy, tokens, 'cold');
        assertDoesNotMatch(matchingStrategy, tokens, 'earn');
        assertDoesNotMatch(matchingStrategy, tokens, 'mail');
    });

    it('should not match when there are too many consecutive unexplained letters.', () => {
        const matchingStrategy = new RegexBasedOrderedMatchingStrategy(1, 10);
        assertDoesNotMatch(matchingStrategy, tokens, 'feature');
        assertDoesNotMatch(matchingStrategy, tokens, 'playback');
        assertDoesNotMatch(matchingStrategy, tokens, 'welcome');
        assertDoesNotMatch(matchingStrategy, tokens, 'zealously');
    });

    it('should not match when there are too many overall unexplained letters.', () => {
        const matchingStrategy = new RegexBasedOrderedMatchingStrategy(3, 3);
        assertDoesNotMatch(matchingStrategy, tokens, 'carapace');
        assertDoesNotMatch(matchingStrategy, tokens, 'playback');
        assertDoesNotMatch(matchingStrategy, tokens, 'zealously');
    });
});

/*
 * Test case for checking the UnorderedMatchingStrategy combined with the selection of all the
 * letters of the words in the considered tokens.
 */
describe('The unordered-based matching strategy used with the all-letters selection strategy', () => {
    const tokens = createDefaultTokens(selectAllLetters);

    it('should explain acronyms as expected', () => {
        const matchingStrategy = new UnorderedMatchingStrategy(10, 15);
        assertMatches(matchingStrategy, tokens, 'CANaPe',
            ['Create Awesome acroNyms Projects', 'Create Amazing acroNyms Projects']);
        assertMatches(matchingStrategy, tokens, 'CARaPace',
            ['Create Awesome acRonyms Projects', 'Create Amazing acRonyms Projects']);
        assertMatches(matchingStrategy, tokens, 'CAviTY',
            ['Create Awesome projecTs acronYms', 'Create Amazing projecTs acronYms']);
        assertMatches(matchingStrategy, tokens, 'CATAlOgUing',
            ['Create Awesome projecTs Acronyms fOr yoUr', 'Create Amazing projecTs Acronyms fOr yoUr']);
        assertMatches(matchingStrategy, tokens, 'CATeRpillaR',
            ['Create Awesome projecTs acRonyms foR', 'Create Amazing projecTs acRonyms foR']);
        assertMatches(matchingStrategy, tokens, 'CATheteRs',
            ['Create Awesome projecTs acRonyms', 'Create Amazing projecTs acRonyms']);
        assertMatches(matchingStrategy, tokens, 'OCEAnOgRaphy',
            ['awesOme Create projEcts Acronyms fOr youR']);
        assertMatches(matchingStrategy, tokens, 'PLAYback',
            ['Projects buiLd Awesome acronYms', 'Projects buiLd Amazing acronYms']);
        assertMatches(matchingStrategy, tokens, 'RATAtOUille',
            ['cReate Awesome projecTs Acronyms fOr yoUr', 'cReate Amazing projecTs Acronyms fOr yoUr']);
        assertMatches(matchingStrategy, tokens, 'SUPeRsatURated',
            ['aweSome bUild Projects acRonyms yoUr foR']);
        assertMatches(matchingStrategy, tokens, 'TElEpAthicallY',
            ['creaTe awEsome projEcts Acronyms Your']);
        assertMatches(matchingStrategy, tokens, 'UNMUSical',
            ['bUild amaziNg acronyMs yoUr projectS']);
        assertMatches(matchingStrategy, tokens, 'WElCOme',
            ['aWesome crEate aCronyms prOjects']);
        assertMatches(matchingStrategy, tokens, 'YEARbOOks',
            ['acronYms crEate Amazing pRojects fOr yOur', 'acronYms crEate Awesome pRojects fOr yOur']);
        assertMatches(matchingStrategy, tokens, 'ZEAlOUsly',
            ['amaZing crEate Acronyms prOjects yoUr']);
    });

    it('should not match when all mandatory tokens cannot be used', () => {
        const matchingStrategy = new UnorderedMatchingStrategy(5, 10);
        assertDoesNotMatch(matchingStrategy, tokens, 'able');
        assertDoesNotMatch(matchingStrategy, tokens, 'act');
        assertDoesNotMatch(matchingStrategy, tokens, 'cold');
        assertDoesNotMatch(matchingStrategy, tokens, 'mail');
    });

    it('should not match when there are too many consecutive unexplained letters.', () => {
        const matchingStrategy = new UnorderedMatchingStrategy(1, 10);
        assertDoesNotMatch(matchingStrategy, tokens, 'caterpillar');
        assertDoesNotMatch(matchingStrategy, tokens, 'oceanography');
        assertDoesNotMatch(matchingStrategy, tokens, 'supersaturated');
        assertDoesNotMatch(matchingStrategy, tokens, 'telepathically');
    });

    it('should not match when there are too many overall unexplained letters.', () => {
        const matchingStrategy = new UnorderedMatchingStrategy(3, 3);
        assertDoesNotMatch(matchingStrategy, tokens, 'cataloguing');
        assertDoesNotMatch(matchingStrategy, tokens, 'oceanography');
        assertDoesNotMatch(matchingStrategy, tokens, 'supersaturated');
        assertDoesNotMatch(matchingStrategy, tokens, 'telepathically');
    });
});
