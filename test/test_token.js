/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const assert = require('assert').strict;
const itParam = require('mocha-param');

const {
    selectAllLetters,
    selectFirstLetter
} = require('../src/selection');

const {
    MatchingToken,
    TokenBuilder
} = require('../src/token');

/**
 * Initializes the Tokens to test.
 *
 * @param {function(string): Set<string>} selectionStrategy The letter
 *        selection strategy to use to create the Tokens.
 *
 * @returns {Token[]} An array with two Tokens, both containing the same
 *          words: the first is mandatory, the second is optional.
 */
function createTokens(selectionStrategy) {
    // Setting up the words of the Tokens.
    const builder = new TokenBuilder(selectionStrategy);
    builder.addWord('Lighten');
    builder.addWord('painstaking');
    builder.addWord('SPOTLESS');

    // Building a mandatory Token.
    const mandatoryToken = builder.build();

    // Building an optional Token.
    builder.setOptional();
    const optionalToken = builder.build();
    return [mandatoryToken, optionalToken];
}

/*
 * Test case for checking the behavior of instances of Token in which only the
 * first letter of each word is selected.
 */
describe('A Token in which only the first letter of each word is selected', () => {
    const tokens = createTokens(selectFirstLetter);
    const [mandatoryToken, optionalToken] = tokens;

    it('should have the expected optionality', () => {
        assert.ok(!mandatoryToken.isOptional());
        assert.ok(optionalToken.isOptional());
    });

    itParam('should contain the expected letters', tokens, (token) => {
        for (const letter of 'lps') {
            assert.ok(token.contains(letter));
        }
    });

    itParam('should provide a view of the expected letters', tokens, (token) => {
        const actualLetters = new Set();
        for (const letter of token.letters()) {
            actualLetters.add(letter);
        }

        for (const letter of 'lps') {
            assert.ok(actualLetters.has(letter));
        }
    });

    itParam('should retrieve the words containing a given letter', tokens, (token) => {
        const withL = token.wordsWith('l');
        assert.equal(withL.size, 1);
        assert.ok(withL.has('lighten'));

        const withP = token.wordsWith('p');
        assert.equal(withP.size, 1);
        assert.ok(withP.has('painstaking'));

        const withS = token.wordsWith('s');
        assert.equal(withS.size, 1);
        assert.ok(withS.has('spotless'));
    });

    itParam('should not contain unexpected letters', tokens, (token) => {
        for (const letter of 'abcdefghijkmnoqrtuvwxyz') {
            assert.ok(!token.contains(letter));
        }
    });

    itParam('should fail when getting words with a non-contained letter', tokens, (token) => {
        for (const letter of 'abcdefghijkmnoqrtuvwxyz') {
            assert.throws(() => token.wordsWith(letter));
        }
    });

    it('should have the expected String representation', () => {
        assert.equal(mandatoryToken.toString(), '(lighten or painstaking or spotless)');
        assert.equal(optionalToken.toString(), '(lighten or painstaking or spotless)?');
    });
});

/*
 * Test case for checking the behavior of instances of Token in which all the
 * letters of each word are selected.
 */
describe('A Token in which all the letters of each word are selected', () => {
    const tokens = createTokens(selectAllLetters);
    const [mandatoryToken, optionalToken] = tokens;

    it('should have the expected optionality', () => {
        assert.ok(!mandatoryToken.isOptional());
        assert.ok(optionalToken.isOptional());
    });

    itParam('should contain the expected letters', tokens, (token) => {
        for (const letter of 'aeghiklnopst') {
            assert.ok(token.contains(letter));
        }
    });

    itParam('should provide a view of the expected letters', tokens, (token) => {
        const actualLetters = new Set();
        for (const letter of token.letters()) {
            actualLetters.add(letter);
        }

        for (const letter of 'aeghiklnopst') {
            assert.ok(actualLetters.has(letter));
        }
    });

    itParam('should retrieve the words containing a given letter', tokens, (token) => {
        const withA = token.wordsWith('a');
        assert.equal(withA.size, 1);
        assert.ok(withA.has('painstaking'));

        const withE = token.wordsWith('e');
        assert.equal(withE.size, 2);
        assert.ok(withE.has('lighten'));
        assert.ok(withE.has('spotless'));

        const withT = token.wordsWith('t');
        assert.equal(withT.size, 3);
        assert.ok(withT.has('lighten'));
        assert.ok(withT.has('painstaking'));
        assert.ok(withT.has('spotless'));
    });

    itParam('should not contain unexpected letters', tokens, (tokenValue) => {
        for (const letter of 'bcdfjmqruvwxyz') {
            assert.ok(!tokenValue.contains(letter));
        }
    });

    itParam('should fail when getting words with a non-contained letter', tokens, (tokenValue) => {
        for (const letter of 'bcdfjmqruvwxyz') {
            assert.throws(() => tokenValue.wordsWith(letter));
        }
    });

    it('should have the expected String representation', () => {
        assert.equal(mandatoryToken.toString(), '(lighten or painstaking or spotless)');
        assert.equal(optionalToken.toString(), '(lighten or painstaking or spotless)?');
    });
});

/*
 * Test case for checking the computation of acronym explanations by instances
 * of MatchingToken.
 */
describe('The explanation computed by a MatchingToken', () => {
    const mandatoryToken = createTokens(selectAllLetters)[0];

    it('should properly explain the letter "h"', () => {
        const matchingToken = new MatchingToken(mandatoryToken, 'h');
        const explanations = matchingToken.getExplanations();
        assert.equal(explanations.length, 1);
        assert.ok(explanations.includes('ligHten'));
    });

    it('should properly explain the letter "i"', () => {
        const matchingToken = new MatchingToken(mandatoryToken, 'i');
        const explanations = matchingToken.getExplanations();
        assert.equal(explanations.length, 2);
        assert.ok(explanations.includes('lIghten'));
        assert.ok(explanations.includes('paInstaking'));
    });

    it('should properly explain the letter "t"', () => {
        const matchingToken = new MatchingToken(mandatoryToken, 't');
        const explanations = matchingToken.getExplanations();
        assert.equal(explanations.length, 3);
        assert.ok(explanations.includes('lighTen'));
        assert.ok(explanations.includes('painsTaking'));
        assert.ok(explanations.includes('spoTless'));
    });
});
