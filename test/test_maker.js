/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

const assert = require('assert').strict;

const AcronymMaker = require('../src/maker');

const {
    GreedyOrderedMatchingStrategy,
    RegexBasedOrderedMatchingStrategy,
    UnorderedMatchingStrategy
} = require('../src/matching');

const {
    selectAllLetters,
    selectFirstLetter
} = require('../src/selection');

/* ---------------- *
 * HELPER FUNCTIONS *
 * ---------------- */

/**
 * Searches for acronyms that are explained by the given tokens.
 *
 * @param {function(string): Set<string>} selectionStrategy The letter selection
 *        strategy to use.
 * @param {MatchingStrategy} matchingStrategy The matching strategy to use.
 * @param {string} tokens The string of the tokens for which to find acronyms.
 *
 * @returns {Acronym[]} The list of found acronyms.
 */
function findAcronyms(selectionStrategy, matchingStrategy, tokens) {
    const acronyms = [];
    const acronymMaker = new AcronymMaker(selectionStrategy, matchingStrategy, (a) => acronyms.push(a));
    acronymMaker.addKnownWords(['able', 'act', 'army', 'canape', 'carapace', 'caterpillar', 'catgut',
        'cavity', 'cold', 'crop', 'earn', 'feature', 'mail', 'oceanography', 'playback', 'ratatouille',
        'supersaturated', 'telepathically', 'unmusical', 'welcome', 'yearbooks', 'zealously']);
    acronymMaker.findAcronymsForString(tokens);
    return acronyms;
}

/**
 * Checks that the given acronym has been explained.
 *
 * @param {string} expectedAcronym The expected acronym that should have been explained.
 * @param {Acronym[]} acronyms The list of acronyms that have been found.
 */
function assertIsExplained(expectedAcronym, acronyms) {
    for (const acronym of acronyms) {
        if (expectedAcronym === acronym.getWord().toLowerCase()) {
            return;
        }
    }
    assert.fail(`Acronym "${expectedAcronym}" is not explained`);
}

/**
 * Checks that the given word has not been explained.
 *
 * @param {string} word The word that should not have been explained.
 * @param {Acronym[]} acronyms The list of acronyms that have been found.
 */
function assertIsNotExplained(word, acronyms) {
    for (const acronym of acronyms) {
        if (word === acronym.getWord().toLowerCase()) {
            assert.fail(`Word "${word}" is explained: ${acronym.toString()}`);
        }
    }
}

/* ---------- *
 * TEST CASES *
 * ---------- */

/*
 * Test case for checking the behavior of an instance of AcronymMaker set up
 * with the letter selection strategy selecting the first letter of the words
 * and the greedy matching strategy respecting the order of the Tokens.
 */
describe('An AcronymMaker with the first-letter selection strategy and the greedy matching strategy', () => {
    const selectionStrategy = selectFirstLetter;
    const matchingStrategy = new GreedyOrderedMatchingStrategy(3, 5);
    const tokens = 'build/create amazing/awesome acronyms for? your? projects';

    it('should explain the expected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assert.equal(acronyms.length, 2);
        assertIsExplained('canape', acronyms);
        assertIsExplained('carapace', acronyms);
    });

    it('should not explain unexpected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assertIsNotExplained('able', acronyms);
        assertIsNotExplained('army', acronyms);
        assertIsNotExplained('catgut', acronyms);
        assertIsNotExplained('cold', acronyms);
        assertIsNotExplained('earn', acronyms);
        assertIsNotExplained('mail', acronyms);
        assertIsNotExplained('playback', acronyms);
        assertIsNotExplained('supersaturated', acronyms);
        assertIsNotExplained('unmusical', acronyms);
        assertIsNotExplained('yearbooks', acronyms);
    });
});

/*
 * Test case for checking the behavior of an instance of AcronymMaker set up
 * with the letter selection strategy selecting the first letter of the words
 * and the regex-based matching strategy respecting the order of the Tokens.
 */
describe('An AcronymMaker with the first-letter selection strategy and the regex-based matching strategy', () => {
    const selectionStrategy = selectFirstLetter;
    const matchingStrategy = new RegexBasedOrderedMatchingStrategy(3, 5);
    const tokens = 'build/create amazing/awesome acronyms for? your? projects';

    it('should explain the expected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assert.equal(acronyms.length, 2);
        assertIsExplained('canape', acronyms);
        assertIsExplained('carapace', acronyms);
    });

    it('should not explain unexpected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assertIsNotExplained('act', acronyms);
        assertIsNotExplained('caterpillar', acronyms);
        assertIsNotExplained('cavity', acronyms);
        assertIsNotExplained('crop', acronyms);
        assertIsNotExplained('feature', acronyms);
        assertIsNotExplained('oceanography', acronyms);
        assertIsNotExplained('ratatouille', acronyms);
        assertIsNotExplained('telepathically', acronyms);
        assertIsNotExplained('welcome', acronyms);
        assertIsNotExplained('zealously', acronyms);
    });
});

/*
 * Test case for checking the behavior of an instance of AcronymMaker set up
 * with the letter selection strategy selecting the first letter of the words
 * and the matching strategy ignoring the order of the Tokens.
 */
describe('An AcronymMaker with the first-letter selection strategy and the unordered matching strategy', () => {
    const selectionStrategy = selectFirstLetter;
    const matchingStrategy = new UnorderedMatchingStrategy(3, 5);
    const tokens = 'build/create amazing/awesome acronyms for? your? projects';

    it('should explain the expected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assert.equal(acronyms.length, 3);
        assertIsExplained('canape', acronyms);
        assertIsExplained('carapace', acronyms);
        assertIsExplained('playback', acronyms);
    });

    it('should not explain unexpected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assertIsNotExplained('able', acronyms);
        assertIsNotExplained('act', acronyms);
        assertIsNotExplained('army', acronyms);
        assertIsNotExplained('catgut', acronyms);
        assertIsNotExplained('cavity', acronyms);
        assertIsNotExplained('earn', acronyms);
        assertIsNotExplained('ratatouille', acronyms);
        assertIsNotExplained('unmusical', acronyms);
        assertIsNotExplained('welcome', acronyms);
        assertIsNotExplained('zealously', acronyms);
    });
});

/*
 * Test case for checking the behavior of an instance of AcronymMaker set up
 * with the letter selection strategy selecting all the letters of the words
 * and the greedy matching strategy respecting the order of the Tokens.
 */
describe('An AcronymMaker with the all-letters selection strategy and the greedy matching strategy', () => {
    const selectionStrategy = selectAllLetters;
    const matchingStrategy = new GreedyOrderedMatchingStrategy(3, 5);
    const tokens = 'build/create amazing/awesome acronyms for? your? projects';

    it('should explain the expected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assert.equal(acronyms.length, 7);
        assertIsExplained('canape', acronyms);
        assertIsExplained('carapace', acronyms);
        assertIsExplained('feature', acronyms);
        assertIsExplained('playback', acronyms);
        assertIsExplained('welcome', acronyms);
        assertIsExplained('yearbooks', acronyms);
        assertIsExplained('zealously', acronyms);
    });

    it('should not explain unexpected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assertIsNotExplained('army', acronyms);
        assertIsNotExplained('caterpillar', acronyms);
        assertIsNotExplained('catgut', acronyms);
        assertIsNotExplained('crop', acronyms);
        assertIsNotExplained('earn', acronyms);
        assertIsNotExplained('oceanography', acronyms);
        assertIsNotExplained('ratatouille', acronyms);
        assertIsNotExplained('supersaturated', acronyms);
        assertIsNotExplained('telepathically', acronyms);
        assertIsNotExplained('unmusical', acronyms);
    });
});

/*
 * Test case for checking the behavior of an instance of AcronymMaker set up
 * with the letter selection strategy selecting all the letters of the words
 * and the regex-based matching strategy respecting the order of the Tokens.
 */
describe('An AcronymMaker with the all-letters selection strategy and the regex-based matching strategy', () => {
    const selectionStrategy = selectAllLetters;
    const matchingStrategy = new RegexBasedOrderedMatchingStrategy(3, 5);
    const tokens = 'build/create amazing/awesome acronyms for? your? projects';

    it('should explain the expected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assert.equal(acronyms.length, 9);
        assertIsExplained('canape', acronyms);
        assertIsExplained('carapace', acronyms);
        assertIsExplained('feature', acronyms);
        assertIsExplained('playback', acronyms);
        assertIsExplained('ratatouille', acronyms);
        assertIsExplained('unmusical', acronyms);
        assertIsExplained('welcome', acronyms);
        assertIsExplained('yearbooks', acronyms);
        assertIsExplained('zealously', acronyms);
    });

    it('should not explain unexpected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assertIsNotExplained('able', acronyms);
        assertIsNotExplained('catgut', acronyms);
        assertIsNotExplained('cavity', acronyms);
        assertIsNotExplained('cold', acronyms);
        assertIsNotExplained('crop', acronyms);
        assertIsNotExplained('earn', acronyms);
        assertIsNotExplained('mail', acronyms);
        assertIsNotExplained('oceanography', acronyms);
        assertIsNotExplained('supersaturated', acronyms);
    });
});

/*
 * Test case for checking the behavior of an instance of AcronymMaker set up
 * with the letter selection strategy selecting all the letters of the words
 * and the matching strategy ignoring the order of the Tokens.
 */
describe('An AcronymMaker with the all-letters selection strategy and the unordered matching strategy', () => {
    const selectionStrategy = selectAllLetters;
    const matchingStrategy = new UnorderedMatchingStrategy(10, 15);
    const tokens = 'build/create amazing/awesome acronyms for? your? projects';

    it('should explain the expected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assert.equal(acronyms.length, 18);
        assertIsExplained('army', acronyms);
        assertIsExplained('canape', acronyms);
        assertIsExplained('carapace', acronyms);
        assertIsExplained('caterpillar', acronyms);
        assertIsExplained('catgut', acronyms);
        assertIsExplained('cavity', acronyms);
        assertIsExplained('crop', acronyms);
        assertIsExplained('earn', acronyms);
        assertIsExplained('feature', acronyms);
        assertIsExplained('oceanography', acronyms);
        assertIsExplained('playback', acronyms);
        assertIsExplained('ratatouille', acronyms);
        assertIsExplained('supersaturated', acronyms);
        assertIsExplained('telepathically', acronyms);
        assertIsExplained('unmusical', acronyms);
        assertIsExplained('welcome', acronyms);
        assertIsExplained('yearbooks', acronyms);
        assertIsExplained('zealously', acronyms);
    });

    it('should not explain unexpected words', () => {
        const acronyms = findAcronyms(selectionStrategy, matchingStrategy, tokens);
        assertIsNotExplained('able', acronyms);
        assertIsNotExplained('act', acronyms);
        assertIsNotExplained('cold', acronyms);
        assertIsNotExplained('mail', acronyms);
    });
});
