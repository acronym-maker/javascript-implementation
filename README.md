# AcronymMaker in JavaScript

[![pipeline status](https://gitlab.com/acronym-maker/javascript-implementation/badges/master/pipeline.svg)](https://gitlab.com/acronym-maker/javascript-implementation/commits/master)
[![coverage report](https://gitlab.com/acronym-maker/javascript-implementation/badges/master/coverage.svg)](https://gitlab.com/acronym-maker/javascript-implementation/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=acronym-maker_javascript-implementation&metric=alert_status)](https://sonarcloud.io/dashboard?id=acronym-maker_javascript-implementation)

## Description

*AcronymMaker* creates awesome acronyms for your projects.
Let us briefly describe how it works with some vocabulary.

A *token* is a set of words from which one word must appear in the acronym
built by *AcronymMaker*.
Said differently, there must be a letter in common between a word from the
set and the built acronym.
This letter may be either the first letter of a word in the token or any
letter, depending on the *letter selection strategy* that is given to
*AcronymMaker*.

Additionally, tokens may be *optional*.
In this case, *AcronymMaker* will try to match a letter from the words in
the optional token to a letter in the acronym, but the acronym will still
be accepted if it fails to do so.

To find an acronym for a given sequence of tokens, *AcronymMaker* uses a
*dictionary*, i.e., a set of known words, in which it looks for acronyms.
A word in the dictionary is said to be *explained* (as an acronym) by the
sequence of tokens if there is a letter in the word for at least one word
in each (non-optional) token.
In this case, we say that the letter is *explained* by the corresponding
word.

Moreover, there are two ways to explain a word as an acronym: either
by following the order of the tokens in the specified sequence, or
without considering this order.
*AcronymMaker* supports both of them (independently).

Finally, note that there may be unexplained letters in the acronym.
Their number may be limited, by limiting both the number of consecutive
unused letters and the number of overall unused letters in a word.
If one of these limits is exceeded, then the word will not be considered
as explained.

## Requirements

This project provides a JavaScript implementation of *AcronymMaker*.
You may run it on your computer using [*Node.js*](https://nodejs.org/).

You may install *AcronymMaker* on your computer along with all its
dependencies thanks to `npm`, with the following command line:

```bash
npm install acronymmaker
```

## How to use *AcronymMaker*

There are two ways to use the JavaScript implementation of *AcronymMaker*.
This section describes both of them.

### Command-Line Interface

*AcronymMaker* comes with a command-line interface that has the following
usage:

```
acronymmaker.js [-l {all,first}] [-m {ordered,ordered-greedy,unordered}] [-c <nb>] [-u <nb>] -d <dict> [<dict> ...]
```

Let us now describe the parameters of the command line above.

+ The parameter `-l` (`--select-letters`) allows specifying whether only the
  `first` letter or `all` the letters of a word from a token may be used to
  explain a letter of the acronym.

+ The parameter `-m` (`--matching-strategy`) allows specifying whether the
  tokens must be considered `ordered` or `unordered`.
  The strategy `ordered-greedy` also considers the tokens in order, but using
  a more efficient algorithm that may however miss matching acronyms which
  would have been found by the `ordered` strategy.

+ The parameters `-c` (`--max-consecutive-unused`) and `-u`
  (`--max-total-unused`) allow specifying the maximum numbers of unused
  letters in the acronym, by limiting the number of consecutive and overall
  unexplained letters, respectively.

+ The parameter `-d` (`--dictionary`) allows specifying the path to the
  dictionary file(s) from which *AcronymMaker* will look for acronyms.
  You may find such dictionaries
  [here](https://gitlab.com/acronym-maker/dictionaries).
  This is the only required parameter.

Once the command-line application has started, a prompt asks you to enter your
tokens, separated by blank spaces.
Each token defines a set of words separated with slashes (`/`), and may end
with a question mark (`?`) to specify that the token is optional.
When you press `Enter`, the matching acronyms are displayed in the console.
You may then enter new sequences of tokens if you wish to find other acronyms,
or you may exit the application by typing either `!exit` or `!quit`.

### JavaScript API

You may also want to directly interact with the JavaScript API of *AcronymMaker*,
for example by requiring *AcronymMaker* in your own *Node.js* modules.
We also provide a browser-friendly bundle of the library, which you can download
[here](https://gitlab.com/acronym-maker/javascript-implementation/builds/artifacts/master/raw/build/acronymmaker.js?job=browser),
if you need *AcronymMaker* in your website, for instance.

In the case you write a *Node.js* module, you first need to require
*AcronymMaker* as shown below.

```javascript
require('acronymmaker');
```

If you use the *browserified* version of *AcronymMaker*, then you only need to
make sure that it is properly loaded (all important functions are made globally
available by *AcronymMaker*).

You can now instantiate an `AcronymMaker` to look for acronyms.
To this end, you first need to specify how acronyms should be retrieved, using
an object specifying all the settings of *AcronymMaker*.

```javascript
const settings = {
    'select-letters': 'first',
    'matching-strategy': 'ordered',
    'max-consecutive-unused': 3,
    'max-total-unused': 5,
    'dictionary': [
        ['foo', 'bar', 'baz'],
        ['tata', 'titi', 'toto', 'tutu'],
    ]
};
```

These settings correspond exactly to those of the command line described in the
previous section.
Note that `dictionary` is an array of arrays, which allows using words from
different sources, for instance if you want to consider different languages
when looking for acronyms.

Then, to create an instance of `AcronymMaker`, you must specify what to do with
the found acronyms.
In the following example, acronyms will be collected in the array `myAcronyms`.

```javascript
const myAcronyms = [];
const maker = createAcronymMaker(settings, (acronym) => myAcronyms.push(acronym));
```

The `maker` initialized above will append to `myAcronyms` all the acronyms
it will identify.
You may of course provide any callback function as second parameter of
`createAcronymMaker()`, for instance to print the found acronym on the
console or to display it in your webpage.

Then, you can find acronyms for a sequence of tokens either by passing a string
representing these tokens (with the same syntax as in the command-line
interface) or with an array of objects representing the tokens.
For instance, you may look for acronyms using

```javascript
maker.findAcronymsForString('foo/bar? baz');
```

or, equivalently, using

```javascript
maker.findAcronymsForObjects([
    { words: ['foo', 'bar'], optional : true },
    { words: ['baz'], optional : false }
]);
```

These methods will try to explain each word of the dictionary, and will
invoke the callback function specified when calling `createAcronymMaker()`
each time it successfully explains a word with the corresponding `Acronym`
instance.
To deal with the instances of `Acronym` that are produced by this method
and stored in the list `myAcronyms` (in this example), you may be
particularly interested in the following methods:

+ `getWord()` gives the word that is explained as an acronym, with
  each explained letter upper-cased.

+ `getExplanations()` gives the array of explanations of the acronym,
  i.e., all the possible combinations of words in the tokens that explain
  the word as an acronym.
  Moreover, each letter corresponding to an explained letter of the acronym
  are upper-cased.
