# Contributing Guidelines

Contributions to this project are welcome!
You will find below useful information if you wish to contribute.

## How is *AcronymMaker* developed?

### Development Tools

This version of *AcronymMaker* is written as a [*Node.js*](https://nodejs.org/)
module using JetBrain's [*WebStorm*](https://www.jetbrains.com/webstorm/)
as IDE.
The configuration files of this IDE are pushed on this repository, so that you
can easily import it in *WebStorm* if you want to.
You are obviously free to use any other IDE, provided that you do not push any
personal configuration file on this repository.

### Development Best Practices

In this project, we try to follow development best practices.

In particular, *AcronymMaker* is heavily tested and its code is statically
analyzed to check its quality and to make sure it follows JavaScript's coding
conventions.

Several scripts are provided in the [`package.json`](package.json) file of
this module to make easier the execution of common tasks.

#### Unit Tests

Unit tests are written with the *Node.js* built-in module
[`assert`](https://nodejs.org/api/assert.html), and are run using
[`mocha`](https://mochajs.org).
Test coverage is measured with [`nyc`](https://istanbul.js.org).
To execute unit tests with code coverage, you first need to install
the development dependencies of *AcronymMaker*, using the following
command line:

```bash
npm install --save-dev
```

Then, you may execute unit tests and test coverage using the `coverage` script:

```bash
npm run coverage
```

Test reports are displayed in the terminal, and are also stored in the
`build` folder.

#### Static Code Analysis

Static code analysis is performed with both [`eslint`](https://eslint.org)
and [*SonarQube*](https://www.sonarqube.org).

To run the `eslint` analysis, type the following command line:

```bash
npm run eslint
```

The violations are stored in the `build` folder.

You may then run the *SonarQube* analysis, by installing
[`sonar-scanner`](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/).
Then, execute the scanner with the following command line (make sure to have
executed both unit tests and the `eslint` analysis before):

```bash
sonar-scanner
```

The report will be uploaded to the *SonarQube* instance specified in your
configuration of `sonar-scanner`.

## Introducing New Features

When using *AcronymMaker*, you may miss some features.
You can submit a *feature request* or a *merge request* for them.

Note that new features have to be useful for most of the users, and should not
introduce (too many) breaking changes.

### Submitting a Feature Request

You can submit a feature request by writing an issue tagged with the label
**Feature Request**.
In this issue, try to describe *precisely* what you want, by using examples if
possible.

We will evaluate the feature you ask for, to see whether it is feasible and
useful for the users.
If so, we will consider its implementation.

### Implementing Features

You may also want to implement the features you miss.
If you want to share your implementation, you will need to fork this project
and to submit a *merge request*.

When writing the merge request, try to describe *precisely* what you have
implemented.
If the feature was initially submitted as a feature request, specify also the
corresponding issue using `#X`, where `X` is the number of the issue.

We will only accept merge requests satisfying the following conditions:

+ The feature you implemented is properly documented to help people understand
  what it does and how to use it.
+ You have written enough unit tests along with your implementation.
+ No breaking changes are introduced, unless your feature requires them.
+ Your implementation is easily readable, and does not trigger any major and
  avoidable linter warning.

If one of these conditions is not satisfied, you will be asked to apply the
changes required to satisfy it.

Note that we may reject some features.
We are open-minded, and rejections are not definitive: we might finally accept
the feature after discussions, or if many users support it.
However, we advise you to write *Feature Requests* first, and to have at most
one implemented feature per merge request (unless they are dependent on each
other), so that we can still accept some of the features while rejecting
others.

## Bug Fixing

While using *AcronymMaker*, you may find bugs that you may want to either
*report* or *fix*.

### Reporting a Bug

To report a bug, just submit an issue tagged with the label **Bug**.

Try to describe as precisely as possible the problem you have encountered.
In particular, we need the following information:

+ What is the expected behaviour?
+ What is the actual behaviour?

If possible, also attach a minimal example so that we can easily reproduce the
bug.

### Fixing a Bug

You may also fix the bug yourself, and submit a merge request with your fix.

Before writing the fix, ask yourself whether the bug you identified is actually
a bug.
If you are not sure, prefer submitting a bug report first.
Otherwise, we will reject the merge request.

When submitting the request, start by describing the bug you have fixed.
If this bug has already been reported, specify the corresponding issue using
`#X`, where `X` is the number of the issue.
Otherwise, describe in your message the bug you have fixed, as for a bug
report.

The conditions for your merge request to be accepted are the following:

+ Again, the bug you fixed is indeed a bug.
+ Unless unavoidable, no breaking changes are introduced.
+ You have written non-regression tests proving that your fix works.
+ Your fix is easily readable, and does not trigger any major and avoidable
  linter warning.

If one of these conditions is not satisfied, you will be asked to apply the
changes required to satisfy it.
