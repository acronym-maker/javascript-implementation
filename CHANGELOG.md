# Changelog

This file describes the evolution of the JavaScript implementation of
*AcronymMaker*.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.1.1 (February 2021)

+ Removes private fields (starting with a `#`) for better compatibility with
  browsers.
+ Improves the documentation.

## Version 0.1.0 (February 2021)

+ Tokens are disjunctions of any number of words.
+ Acronyms are made from conjunctions of any number of tokens.
+ Letters used in acronyms can be either the first or any letter from a word.
+ Order of tokens can either be taken into account or not.
+ Tokens may be optional.
