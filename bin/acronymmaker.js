#!/usr/bin/env node

/* -------------------------------------------------------------------------- *
 *                                                                            *
 *   AcronymMaker - Create awesome acronyms for your projects!                *
 *   Copyright (c) 2020-2021 - Romain Wallon (romain.gael.wallon@gmail.com)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *   See the GNU General Public License for more details.                     *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.                                                 *
 *   If not, see <https://www.gnu.org/licenses/>.                             *
 *                                                                            *
 * -------------------------------------------------------------------------- */

/* eslint-disable no-console */

const figlet = require('figlet');
const fs = require('fs');
const prompt = require('prompt-sync')({ sigint: true });
const yargs = require('yargs/yargs');

require('../src');

/* --------- *
 * FUNCTIONS *
 * --------- */

/**
 * Parses the command line arguments.
 *
 * @returns {object} The options given to AcronymMaker.
 */
function parseArguments() {
    // Initializing the parser.
    const args = yargs(process.argv.slice(2));
    args.scriptName('acronymmaker.js');
    args.usage('acronymmaker.js [options]');

    // Registering the option used to display the help of the program.
    args.help();
    args.alias('help', 'h');
    args.wrap(args.terminalWidth());

    // Registering the option used to display the version of the program.
    args.version('AcronymMaker (Node.js) - Version 0.1.1');
    args.alias('version', 'v');

    // Registering the option used to set the letter selection strategy to use.
    args.option('select-letters', {
        alias: 'l',
        describe: 'Specifies the letter selection strategy to use',
        choices: selections,
        default: 'all',
        type: 'string'
    });

    // Registering the option used to set the matching strategy to use.
    args.option('matching-strategy', {
        alias: 'm',
        describe: 'Specifies the matching strategy to use',
        choices: matchings,
        default: 'ordered',
        type: 'string'
    });

    // Registering the option used to set the maximum number of consecutive unused letters.
    args.option('max-consecutive-unused', {
        alias: 'c',
        describe: 'Specifies the maximum number of consecutive unused letters allowed in an acronym',
        default: 3,
        type: 'number'
    });

    // Registering the option used to set the maximum number of total unused letters.
    args.option('max-total-unused', {
        alias: 'u',
        describe: 'Specifies the maximum number of total unused letters allowed in an acronym',
        default: 5,
        type: 'number'
    });

    // Registering the option used to specify the input dictionaries.
    args.option('dictionary', {
        alias: 'd',
        describe: 'Paths to input dictionaries',
        demandOption: true,
        type: 'array'
    });

    return args.argv;
}

/**
 * Prints the program's header, showing the name of the program and the
 * instructions for using it.
 */
function printHeader() {
    // Showing the name of the program.
    const title = figlet.textSync('AcronymMaker');
    console.log(title);

    // Showing the instructions.
    console.log();
    console.log('Enter the tokens to find acronyms for (there is no word limit).');
    console.log('For example, type "foo/bar? baz" if you want an acronym for either');
    console.log('"foo baz", "bar baz" or "baz" (the "?" marks optional tokens).');
    console.log();
}

/* ---- *
 * MAIN *
 * ---- */

// Initializing the application.
const cmdArguments = parseArguments();
const maker = createAcronymMaker(cmdArguments,
    (acronym) => console.log(acronym.toString()),
    (dict) => fs.readFileSync(dict, 'utf-8').split('\n'));

// Showing the name of the program and its instructions.
printHeader();

// Starting the REPL.
for (let exited = false; !exited;) {
    // Reading the tokens.
    const tokens = prompt('> ').trim();

    if ((tokens === '!exit') || (tokens === '!quit')) {
        // Exiting the application.
        console.log('Bye!');
        exited = true;

    } else if (tokens.length > 0) {
        // Computing the acronyms for the tokens.
        maker.findAcronymsForString(tokens);
        console.log();
    }
}
